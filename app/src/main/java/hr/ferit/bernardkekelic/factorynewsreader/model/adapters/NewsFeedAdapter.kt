package hr.ferit.bernardkekelic.factorynewsreader.model.adapters

import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import hr.ferit.bernardkekelic.factorynewsreader.R
import hr.ferit.bernardkekelic.factorynewsreader.context.MyApplication
import hr.ferit.bernardkekelic.factorynewsreader.model.api.newsFeed.Article
import hr.ferit.bernardkekelic.factorynewsreader.model.interactionListeners.HomeItemInteractionListener
import kotlinx.android.synthetic.main.item_home_search_result.view.*

class NewsFeedAdapter(homeItemListener: HomeItemInteractionListener) : RecyclerView.Adapter<NewsFeedHolder>() {

    private val results: MutableList<Article> = mutableListOf()
    private val homeItemListener: HomeItemInteractionListener = homeItemListener

    fun addData(newResults: List<Article>) {
        results.clear()
        results.addAll(newResults)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsFeedHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_home_search_result, parent, false)
        return NewsFeedHolder(view)
    }

    override fun getItemCount(): Int = results.size

    override fun onBindViewHolder(viewHolder: NewsFeedHolder, position: Int) {
        val newsItem = results[position]
        viewHolder.bind(newsItem, homeItemListener)
    }

}

class NewsFeedHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(result: Article, homeItemListener: HomeItemInteractionListener) {
        itemView.singleNews_subtitle.text = result.articleTitle
        if (result.articleUrlToImage.isNotEmpty()) {
            Picasso.get()
                .load(result.articleUrlToImage)
                .resize(
                    MyApplication.ApplicationContext.resources.getInteger(R.integer.homeItem_thumbnailSizeInt),
                    MyApplication.ApplicationContext.resources.getInteger(R.integer.homeItem_thumbnailSizeInt)
                )
                .centerCrop()
                .into(itemView.homeItem_thumbnail)
        }

        itemView.setOnClickListener { homeItemListener.onClick(result) }

        val myShader = LinearGradient(
            0F,
            42F,
            0F,
            100F,
            Color.BLACK,
            Color.parseColor(MyApplication.ApplicationContext.getString(R.string.homeItem_titleGradientStartColor)),
            Shader.TileMode.CLAMP
        )
        itemView.singleNews_subtitle.paint.shader = myShader

    }

}
