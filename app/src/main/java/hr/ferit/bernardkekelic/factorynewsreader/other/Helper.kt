package hr.ferit.bernardkekelic.factorynewsreader.other

import java.text.SimpleDateFormat
import java.util.*

fun getCurrentDateAndTime(): String {
    val dateFormat = SimpleDateFormat(TIME_FORMAT_PATTERN)
    return dateFormat.format(Date())
}


