package hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface LocalArticleDao {

    @Insert
    fun insert(article: LocalArticle)

    @Delete
    fun delete(article: LocalArticle)

    @Query("SELECT * FROM articles")
    fun getAll(): List<LocalArticle>

    @Query("SELECT * FROM articles WHERE url = :articleUrl")
    fun getByArticleUrl(articleUrl: String): LocalArticle

    @Query("DELETE FROM articles")
    fun deleteAllArticles()

    @Query("UPDATE articles SET pageText = :downloadedPageText WHERE :receivedArticleUrl = url")
    fun updatePageText(downloadedPageText: String, receivedArticleUrl: String)

    @Query("SELECT LENGTH(pageText) FROM articles WHERE :receivedArticleUrl = url")
    fun checkPageTextLength(receivedArticleUrl: String): Int
}