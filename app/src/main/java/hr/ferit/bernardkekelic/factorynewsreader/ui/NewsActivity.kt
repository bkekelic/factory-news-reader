package hr.ferit.bernardkekelic.factorynewsreader.ui

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import hr.ferit.bernardkekelic.factorynewsreader.R
import hr.ferit.bernardkekelic.factorynewsreader.context.MyApplication
import hr.ferit.bernardkekelic.factorynewsreader.model.adapters.SingleNewsAdapter
import hr.ferit.bernardkekelic.factorynewsreader.model.fragments.SingleNewsFragment
import hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase.LocalArticleDatabase
import kotlinx.android.synthetic.main.activity_news.*

class NewsActivity : AppCompatActivity() {

    companion object {
        const val KEY: String = "url"
        const val DEFAULT: String = "none"
        var selectedPage: Int = 0
    }

    private val localArticleDao = LocalArticleDatabase.getInstance().articleDao()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        setUpUI()
    }

    private fun setUpUI() {

        actionBar_backButton.setOnClickListener { goBackToHomeActivity() }
        val receivedUrl = intent?.getStringExtra(KEY) ?: DEFAULT

        singleNews_viewPager.adapter = SingleNewsAdapter(supportFragmentManager)

        singleNews_viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(position: Int) {
                SingleNewsFragment.pageSwitched = true
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}

            override fun onPageSelected(position: Int) {
                selectedPage = position
                actionBar_singleNewsTitle.text = getRightArticleTitleForFragment(position)
            }
        })

        showRightFragment(receivedUrl)
    }

    private fun goBackToHomeActivity() {
        val intent = Intent(MyApplication.ApplicationContext, HomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }


    private fun showRightFragment(receivedUrl: String) {
        val localArticles = localArticleDao.getAll()
        for ((place, article) in localArticles.withIndex()) {
            if (article.articleUrl == receivedUrl) {
                selectedPage = place
                actionBar_singleNewsTitle.text = article.articleTitle
                singleNews_viewPager.currentItem = place
                break
            }
        }
    }

    private fun getRightArticleTitleForFragment(position: Int): String {
        val localArticles = localArticleDao.getAll()
        for ((place, article) in localArticles.withIndex()) {
            if (place == position) {
                return article.articleTitle
            }
        }
        return ""
    }


}
