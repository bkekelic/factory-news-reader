package hr.ferit.bernardkekelic.factorynewsreader.model.interactionListeners

import hr.ferit.bernardkekelic.factorynewsreader.model.api.newsFeed.Article

interface HomeItemInteractionListener {
    fun onClick(clickedArticle: Article)
}