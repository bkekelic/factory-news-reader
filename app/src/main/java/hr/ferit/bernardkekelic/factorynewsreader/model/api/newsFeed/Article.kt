package hr.ferit.bernardkekelic.factorynewsreader.model.api.newsFeed

import com.google.gson.annotations.SerializedName

data class Article(
    @SerializedName("author") var articleAuthor: String = "",
    @SerializedName("title") var articleTitle: String = "",
    @SerializedName("description") var articleDescription: String = "",
    @SerializedName("url") var articleUrl: String = "",
    @SerializedName("urlToImage") var articleUrlToImage: String = "",
    @SerializedName("publishedAt") var articlePublishedAt: String = ""
)
