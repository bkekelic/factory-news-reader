package hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import hr.ferit.bernardkekelic.factorynewsreader.context.MyApplication

@Database(version = 1, entities = [LocalArticle::class, MyLogcat::class])
abstract class LocalArticleDatabase : RoomDatabase() {

    abstract fun articleDao(): LocalArticleDao
    abstract fun logcatDao(): MyLogcatDao

    companion object {
        private const val NAME = "article database"
        private var INSTANCE: LocalArticleDatabase? = null

        fun getInstance(): LocalArticleDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    MyApplication.ApplicationContext,
                    LocalArticleDatabase::class.java,
                    NAME
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE as LocalArticleDatabase
        }
    }

}
