package hr.ferit.bernardkekelic.factorynewsreader.model.api.service

import hr.ferit.bernardkekelic.factorynewsreader.model.api.newsFeed.NewsFeedResultRootObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface NewsFeedApi {

    @GET("v1/articles")
    fun findNews(
        @Query("source") source: String,
        @Query("sortBy") sortBy: String,
        @Query("apiKey") apiKey: String
    ): Call<NewsFeedResultRootObject>
}