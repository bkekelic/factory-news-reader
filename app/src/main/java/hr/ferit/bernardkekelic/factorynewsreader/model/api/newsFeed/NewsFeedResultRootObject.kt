package hr.ferit.bernardkekelic.factorynewsreader.model.api.newsFeed

import com.google.gson.annotations.SerializedName

data class NewsFeedResultRootObject(
    @SerializedName("status") val status: String,
    @SerializedName("source") val source: String,
    @SerializedName("sortBy") val sortBy: String,
    @SerializedName("articles") val articles: List<Article>
)