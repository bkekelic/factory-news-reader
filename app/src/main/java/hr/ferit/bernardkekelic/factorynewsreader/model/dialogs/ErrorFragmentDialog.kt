package hr.ferit.bernardkekelic.factorynewsreader.model.dialogs

import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button


class ErrorFragmentDialog : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(hr.ferit.bernardkekelic.factorynewsreader.R.layout.dialog_error, container, false)

        val errorBtn: Button = view.findViewById(hr.ferit.bernardkekelic.factorynewsreader.R.id.errorDialog_button)
        errorBtn.setOnClickListener { dialog.dismiss() }

        return view
    }

    override fun onStart() {
        super.onStart()
        if (dialog == null) return

        val wm = context!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x - 60

        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = width
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes = lp
    }
}