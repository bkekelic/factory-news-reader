package hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "myLogcat")
data class MyLogcat(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name = "lastWriteTime") var lastWriteTime: String
)