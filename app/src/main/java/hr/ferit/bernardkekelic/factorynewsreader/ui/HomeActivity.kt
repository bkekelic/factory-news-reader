package hr.ferit.bernardkekelic.factorynewsreader.ui

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import hr.ferit.bernardkekelic.factorynewsreader.R
import hr.ferit.bernardkekelic.factorynewsreader.context.MyApplication
import hr.ferit.bernardkekelic.factorynewsreader.model.adapters.NewsFeedAdapter
import hr.ferit.bernardkekelic.factorynewsreader.model.api.networking.Networking
import hr.ferit.bernardkekelic.factorynewsreader.model.api.newsFeed.Article
import hr.ferit.bernardkekelic.factorynewsreader.model.api.newsFeed.NewsFeedResultRootObject
import hr.ferit.bernardkekelic.factorynewsreader.model.fragments.SingleNewsFragment
import hr.ferit.bernardkekelic.factorynewsreader.model.interactionListeners.HomeItemInteractionListener
import hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase.LocalArticle
import hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase.LocalArticleDatabase
import hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase.MyLogcat
import hr.ferit.bernardkekelic.factorynewsreader.other.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.dialog_error.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class HomeActivity : AppCompatActivity(), Callback<NewsFeedResultRootObject> {

    private val localArticleDao = LocalArticleDatabase.getInstance().articleDao()
    private val myLogcatDao = LocalArticleDatabase.getInstance().logcatDao()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setUpUi()
        checkDatabaseForArticles()
    }


    private fun setUpUi() {

        // Init recyclerView
        homeResultsView.layoutManager =
            LinearLayoutManager(MyApplication.ApplicationContext, RecyclerView.VERTICAL, false)
        homeResultsView.itemAnimator = DefaultItemAnimator()

        // On article item click
        val homeItemListener = object : HomeItemInteractionListener {
            override fun onClick(clickedArticle: Article) {
                // Reset fragment state
                SingleNewsFragment.pageSwitched = false
                SingleNewsFragment.shownOnce = false

                goToSingleNews(clickedArticle)
            }
        }

        // Show in recyclerView
        homeResultsView.adapter = NewsFeedAdapter(homeItemListener)

        // On pull down to refresh
        swipe_refresh_layout.setOnRefreshListener {
            progressBar_home.visibility = View.VISIBLE
            deleteDataFromDatabase()
            findNews()
        }
    }

    private fun checkDatabaseForArticles() {

        progressBar_home.visibility = View.VISIBLE

        // There is already some stored news
        if (myLogcatDao.checkIfThereIsAlreadyInputInDatabase() != 0) {
            checkIfSpecifiedTimeHasPassed()

            // No news stored yet
        } else {
            findNews()
        }

    }


    private fun findNews() {
        Networking.newsFeedService.findNews(
            API_SOURCE,
            API_SORT_BY,
            API_KEY
        ).enqueue(this)
    }

    override fun onFailure(call: Call<NewsFeedResultRootObject>, t: Throwable) {
        Log.d("TAG", t.message.toString())
        removeRefreshIndicator()
        fillAndRemoveProgressBar()
        showErrorDialog()
    }

    override fun onResponse(call: Call<NewsFeedResultRootObject>, response: Response<NewsFeedResultRootObject>) {

        fillAndRemoveProgressBar()

        val result = response.body()

        // Add articles to list
        val listOfArticles: MutableList<Article> = mutableListOf()
        for (article in result!!.articles) {
            listOfArticles.add(article)
        }

        // Show in recycler view
        (homeResultsView.adapter as NewsFeedAdapter).addData(result.articles)
        removeRefreshIndicator()

        // Save to Room
        saveArticlesToDatabase(listOfArticles)
    }


    private fun checkIfSpecifiedTimeHasPassed() {

        val dateFormat = SimpleDateFormat(TIME_FORMAT_PATTERN)
        try {
            // Get current and added time
            val convertedCurrentDateAndTime = dateFormat.parse(getCurrentDateAndTime())
            val addedTime = Calendar.getInstance()
            addedTime.time = dateFormat.parse(myLogcatDao.getLastLogcat().lastWriteTime)
            addedTime.add(Calendar.MINUTE, TIME_DIFFERENCE)
            val convertedTargetTime = addedTime.time

            // 5 minutes passed
            if (convertedCurrentDateAndTime.after(convertedTargetTime)) {
                deleteDataFromDatabase()
                findNews()

                // 5 minutes didn't pass
            } else {
                showArticlesFromDatabase()
            }

        } catch (e: ParseException) {
            fillAndRemoveProgressBar()
            Log.d("TAG", e.message.toString())
        }
    }

    private fun deleteDataFromDatabase() {
        localArticleDao.deleteAllArticles()
    }

    private fun showArticlesFromDatabase() {
        val articles: MutableList<Article> = mutableListOf()

        // Get data from database
        val localArticles = localArticleDao.getAll()
        for (localArticle in localArticles) {
            val article = Article(
                articleTitle = localArticle.articleTitle,
                articleUrlToImage = localArticle.articleUrlToImage,
                articlePublishedAt = localArticle.articlePublishedAt,
                articleUrl = localArticle.articleUrl
            )
            articles.add(article)
        }
        fillAndRemoveProgressBar()

        // Show in recycler view
        (homeResultsView.adapter as NewsFeedAdapter).addData(articles)

        removeRefreshIndicator()
    }

    private fun saveArticlesToDatabase(articles: List<Article>) {

        for (article in articles) {
            val localArticle = LocalArticle(
                articleTitle = article.articleTitle,
                articleUrl = article.articleUrl,
                articleUrlToImage = article.articleUrlToImage,
                articlePublishedAt = article.articlePublishedAt
            )
            localArticleDao.insert(localArticle)
        }
        val myLogcat = MyLogcat(lastWriteTime = getCurrentDateAndTime())

        if (myLogcatDao.checkIfThereIsAlreadyInputInDatabase() == 0) {
            myLogcatDao.insert(myLogcat)
        } else {
            myLogcatDao.updateTime(myLogcat.lastWriteTime)
        }
    }


    private fun goToSingleNews(clickedArticle: Article) {

        SingleNewsFragment.clickedArticleUrl = clickedArticle.articleUrl

        val intent = Intent(MyApplication.ApplicationContext, NewsActivity::class.java)
        intent.putExtra(NewsActivity.KEY, clickedArticle.articleUrl)
        startActivity(intent)
    }


    private fun removeRefreshIndicator() {
        if (swipe_refresh_layout.isRefreshing)
            swipe_refresh_layout.isRefreshing = false
    }

    private fun showErrorDialog() {
        val customErrorDialogView =
            LayoutInflater.from(this).inflate(R.layout.dialog_error, null)

        val dialogBuilder = AlertDialog.Builder(this).setView(customErrorDialogView)

        val customErrorDialog = dialogBuilder.show()

        customErrorDialogView.errorDialog_button.setOnClickListener { customErrorDialog.dismiss() }
    }

    private fun fillAndRemoveProgressBar() {
        progressBar_home.max = 100
        doAsync {
            for (i in (30..100)) {
                Thread.sleep(10)
                uiThread {
                    progressBar_home.progress += 1
                }
            }
            uiThread {
                progressBar_home.visibility = View.GONE
            }
        }
    }
}
