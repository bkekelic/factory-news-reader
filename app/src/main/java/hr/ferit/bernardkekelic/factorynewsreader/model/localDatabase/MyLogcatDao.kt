package hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface MyLogcatDao {

    @Insert
    fun insert(customLogcat: MyLogcat)

    @Query("SELECT * FROM myLogcat")
    fun getLastLogcat(): MyLogcat

    @Query("SELECT count(id) FROM myLogcat")
    fun checkIfThereIsAlreadyInputInDatabase(): Int

    @Query("UPDATE myLogcat SET lastWriteTime = :time")
    fun updateTime(time: String)
}