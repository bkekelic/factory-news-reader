package hr.ferit.bernardkekelic.factorynewsreader.model.api.networking

import hr.ferit.bernardkekelic.factorynewsreader.model.api.service.NewsFeedApi
import hr.ferit.bernardkekelic.factorynewsreader.other.API_BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Networking {
    val newsFeedService: NewsFeedApi = Retrofit.Builder()
        .addConverterFactory(ConverterFactory.converterFactory)
        .client(HttpClient.client)
        .baseUrl(API_BASE_URL)
        .build()
        .create(NewsFeedApi::class.java)
}

object ConverterFactory {
    val converterFactory = GsonConverterFactory.create()
}

object HttpClient {
    val client = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()
}