package hr.ferit.bernardkekelic.factorynewsreader.model.adapters


import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import hr.ferit.bernardkekelic.factorynewsreader.model.fragments.SingleNewsFragment
import hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase.LocalArticleDatabase

class SingleNewsAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val articles = LocalArticleDatabase.getInstance().articleDao().getAll()

    val fragments = arrayOf(
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance(),
        SingleNewsFragment.newInstance()
    )

    val titles = arrayOf("0", "1", "2", "3", "4", "5", "6", "7", "8", "9")

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }


}