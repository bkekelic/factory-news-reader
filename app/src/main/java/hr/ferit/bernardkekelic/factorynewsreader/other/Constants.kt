package hr.ferit.bernardkekelic.factorynewsreader.other

// api constants
const val API_BASE_URL = "https://newsapi.org/"
const val API_SOURCE = "bbc-news"
const val API_SORT_BY = "top"
const val API_KEY = "6946d0c07a1c4555a4186bfcade76398"

const val TIME_DIFFERENCE = 5 //minutes
const val TIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss"