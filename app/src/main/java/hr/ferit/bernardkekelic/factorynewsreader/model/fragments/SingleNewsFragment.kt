package hr.ferit.bernardkekelic.factorynewsreader.model.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.squareup.picasso.Picasso
import hr.ferit.bernardkekelic.factorynewsreader.R
import hr.ferit.bernardkekelic.factorynewsreader.model.dialogs.ErrorFragmentDialog
import hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase.LocalArticleDatabase
import hr.ferit.bernardkekelic.factorynewsreader.ui.NewsActivity
import kotlinx.android.synthetic.main.activity_news.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.jsoup.Jsoup
import java.io.IOException

class SingleNewsFragment : Fragment() {


    companion object {
        fun newInstance(): SingleNewsFragment {
            return SingleNewsFragment()
        }

        var pageSwitched = false
        var shownOnce = false
        var clickedArticleUrl: String = ""
    }

    lateinit var progressBar: ProgressBar
    private val localArticleDao = LocalArticleDatabase.getInstance().articleDao()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_single_news, container, false)
        progressBar = view.findViewById(R.id.progressBar_news)
        progressBar.progress = 30
        // Just for first opened fragment from homepage
        if (!shownOnce) {
            getLocalData(view)
            if (localArticleDao.checkPageTextLength(clickedArticleUrl) == 0) downloadPageText(clickedArticleUrl)
        }
        return view
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (pageSwitched) {
                try {
                    val title: TextView = view!!.findViewById(R.id.singleNews_subtitle)
                    val pageText: TextView = view!!.findViewById(R.id.singleNews_pageText)
                    val image: ImageView = view!!.findViewById(R.id.singleNews_image)


                    val articles = localArticleDao.getAll()
                    val chosenArticle = localArticleDao.getByArticleUrl(articles[NewsActivity.selectedPage].articleUrl)

                    title.text = chosenArticle.articleTitle

                    if (localArticleDao.checkPageTextLength(chosenArticle.articleUrl) == 0) downloadPageText(
                        chosenArticle.articleUrl
                    )
                    else pageText.text = chosenArticle.pageText


                    if (chosenArticle.articleUrlToImage.isNotEmpty()) {
                        Picasso.get()
                            .load(chosenArticle.articleUrlToImage)
                            .into(image)
                    }

                } catch (e: Exception) {
                    showErrorDialog()
                }

            }
        }
    }

    private fun downloadPageText(articleUrl: String) {
        val builder = StringBuilder()
        progressBar.visibility = View.VISIBLE
        doAsync {
            try {
                val doc = Jsoup.connect(articleUrl).get()
                val paragraphs = doc.select("div > p")
                val pageText: TextView = view!!.findViewById(R.id.singleNews_pageText)

                var stepper = 70 / paragraphs.size

                for (p in paragraphs) {
                    if (checkForNonParagraphStringsInHtmlDoc(p.text())) {
                        builder.append(p.text()).append("\n\n")
                        uiThread {
                            progressBar.progress += stepper
                        }
                    }
                }
                uiThread {
                    try {
                        pageText.text = builder
                        localArticleDao.updatePageText(builder.toString(), articleUrl)
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        showErrorDialog()
                    }

                }

            } catch (e: IOException) {
                showErrorDialog()
            }

        }
    }

    private fun getLocalData(view: View) {
        shownOnce = true
        try {
            val articles = localArticleDao.getAll()
            val chosenArticle = localArticleDao.getByArticleUrl(articles[NewsActivity.selectedPage].articleUrl)

            val pageTitle: TextView = view.findViewById(R.id.singleNews_subtitle)
            val pageText: TextView = view.findViewById(R.id.singleNews_pageText)
            val pageImage: ImageView = view.findViewById(R.id.singleNews_image)

            pageTitle.text = chosenArticle.articleTitle
            pageText.text = chosenArticle.pageText

            if (chosenArticle.articleUrlToImage.isNotEmpty()) {
                Picasso.get()
                    .load(chosenArticle.articleUrlToImage)
                    .into(pageImage)
            }
        } catch (e: Exception) {
            showErrorDialog()
        }
    }

    // Returns true if there is no extra strings
    private fun checkForNonParagraphStringsInHtmlDoc(text: String): Boolean {
        val nonParagraphs = resources.getStringArray(R.array.nonParagraphStrings)
        for (nonP in nonParagraphs) {
            if (text == nonP) return false
        }
        return true
    }

    private fun showErrorDialog() {
        val errorDialog = ErrorFragmentDialog()
        errorDialog.show(fragmentManager, "ErrorFragmentDialog")
    }
}