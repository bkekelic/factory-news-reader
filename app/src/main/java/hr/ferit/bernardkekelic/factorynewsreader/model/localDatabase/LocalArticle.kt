package hr.ferit.bernardkekelic.factorynewsreader.model.localDatabase

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "articles")
data class LocalArticle(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name = "title") var articleTitle: String,
    @ColumnInfo(name = "url") var articleUrl: String,
    @ColumnInfo(name = "urlToImage") var articleUrlToImage: String,
    @ColumnInfo(name = "publishedAt") var articlePublishedAt: String,
    @ColumnInfo(name = "pageText") var pageText: String = ""
)